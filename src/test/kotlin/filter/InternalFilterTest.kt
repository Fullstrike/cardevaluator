package filter

import org.junit.Test

class InternalFilterTest {
    private val filter = InternalFilter()

    @Test
    fun `valid card number`() {
        val cardNumber = "4929804463622139"
        assert(filter.filter(cardNumber).isCardValid)
        assert(filter.filter(cardNumber).isLuhnCheckPassed)
    }

    @Test
    fun `fail luhn check`() {
        val cardNumber = "5212132012291762"
        assert(filter.filter(cardNumber).isCardValid.not())
        assert(filter.filter(cardNumber).isLuhnCheckPassed.not())
    }

    @Test
    fun `card number with leading zero`() {
        val cardNumber = "0012132012291762"
        assert(filter.filter(cardNumber).isCardValid.not())
        assert(filter.filter(cardNumber).isLuhnCheckPassed.not())
    }

    @Test
    fun `empty card number`() {
        val cardNumber = ""
        val result = filter.filter(cardNumber)
        assert(result.number.isEmpty())
        assert(result.isCardValid.not())
    }

    @Test
    fun `card type visa`() {
        val cardNumber = "4556866775271061"
        assert(filter.filter(cardNumber).network == "visa")
    }

    @Test
    fun `card type mastercard`() {
        val cardNumber = "5262545317938195"
        assert(filter.filter(cardNumber).network == "mastercard")
    }
}