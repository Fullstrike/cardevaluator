import filter.Filter
import model.CardInfo
import org.junit.Before
import org.junit.Test
import java.lang.RuntimeException

class EvaluatorTest {
    private val firstFilter = StubFilter()
    private val secondFilter = StubFilter()

    private val evaluator = EvaluatorImpl(
        listOf(
            firstFilter,
            secondFilter
        )
    )

    @Before
    fun setup() {
        firstFilter.passValue = false
        secondFilter.passValue = false
    }

    @Test
    fun `first filter success`() {
        firstFilter.passValue = true

        evaluator.getCardsInfo(
            listOf(
                "1",
                "2"
            )
        ).forEach {
            assert(it.isCardValid)
        }
    }

    @Test
    fun `first filter failed`() {
        secondFilter.passValue = true

        evaluator.getCardsInfo(
            listOf(
                "1",
                "2"
            )
        ).forEach {
            assert(it.isCardValid)
        }
    }

    @Test
    fun `all filters failed`() {
        evaluator.getCardsInfo(
            listOf(
                "1",
                "2"
            )
        ).forEach {
            assert(it.isCardValid.not())
        }
    }

    @Test
    fun `empty card list`() {
        assert(evaluator.getCardsInfo(emptyList()).isEmpty())
    }

    @Test
    fun `valid cards filter success`() {
        val result = evaluator.getValidCards(
            listOf(
                "3",
                "4",
                "5"
            )
        )

        assert(result.size == 2)
        result.forEach {
            assert(it.isCardValid)
        }
    }

    @Test
    fun `cards filtered by visa network name`() {
        val result = evaluator.getCardsByNetworkName(
            listOf(
                "3",
                "4",
                "5"
            ), "visa"
        )

        assert(result.size == 1)
        assert(result.first().network == "visa")
    }

    @Test
    fun `cards filtered by predicate`() {
        val result = evaluator.getCardsByPredicate(
            listOf(
                "3",
                "4",
                "5",
                "6"
            )
        ) {
            it.network == "visa" && it.isCardValid
        }

        assert(result.size == 1)
        assert(result.first().network == "visa")
        assert(result.first().isCardValid)
    }
}

internal class StubFilter(var passValue: Boolean = false) : Filter() {
    override fun filter(cardNumber: String): CardInfo {
        if (passValue) {
            return CardInfo(cardNumber, isCardValid = true)
        }

        return when(cardNumber) {
            "4" -> CardInfo(cardNumber, isCardValid = false, network = "visa")
            "3", "5" -> CardInfo(cardNumber, isCardValid = true)
            "6" -> CardInfo(cardNumber, isCardValid = true, network = "visa")
            else -> throw RuntimeException()
        }
    }
}