package network

import java.net.URL

internal class CardInfoFetcher {

    fun fetchInfo(cardNumber: String): String {
        val request = "$API_URL$cardNumber"
        println("making request $request")
        return URL(request).readText()
    }

    companion object {
        private const val API_URL = "https://lookup.binlist.net/"
    }
}