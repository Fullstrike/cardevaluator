import com.google.gson.Gson
import filter.Filter
import filter.InternalFilter
import filter.NetworkFilter
import mapper.CardInfoMapper
import model.CardInfo
import network.CardInfoFetcher
import java.lang.Exception
import java.util.concurrent.Executors

interface Evaluator {
    fun getCardsInfo(cardNumbers: Iterable<String>): List<CardInfo>

    fun getValidCards(cardNumbers: Iterable<String>): List<CardInfo>

    fun getCardsByNetworkName(cardNumbers: Iterable<String>, network: String): List<CardInfo>

    fun getCardsByPredicate(cardNumbers: Iterable<String>, filter: (CardInfo) -> Boolean): List<CardInfo>

    companion object {
        private var evaluator: Evaluator? = null

        fun create(): Evaluator {

            if (evaluator != null) {
                return evaluator!!
            }
            evaluator = EvaluatorImpl(
                listOf(
                    provideNetworkFilter(),
                    provideInternalFilter()
                )
            )

            return evaluator!!
        }

        private fun provideGson(): Gson {
            return Gson()
        }

        private fun provideCardInfoMapper(): CardInfoMapper {
            return CardInfoMapper(provideGson())
        }

        private fun provideFetcher() : CardInfoFetcher {
            return CardInfoFetcher()
        }

        private fun provideInternalFilter(): Filter {
            return InternalFilter()
        }

        private fun provideNetworkFilter(): Filter {
            return NetworkFilter(provideFetcher(), provideCardInfoMapper())
        }

    }
}

internal class EvaluatorImpl(
    private val filters: List<Filter>
) : Evaluator {

    private val executor = Executors.newCachedThreadPool()

    /**
     * Blocking request to get and filter card info by several sources.
     * If the filter throws an exception, the result of the next filter will be taken.
     *
     * @param cardNumbers - card numbers that need to be filtered.
     * @return list of card info objects {@link model.CardInfo}.
     */
    override fun getCardsInfo(cardNumbers: Iterable<String>): List<CardInfo> {
        return cardNumbers.asSequence().map {
            for (filter in filters) {
                try {
                    return@map executor.submit<CardInfo> { filter.filter(it) }.get()
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            return@map CardInfo(it)
        }.toList()
    }

    /**
     * Blocking request to get and filter card info by card validity factor.
     *
     * @param cardNumbers - card numbers that need to be filtered.
     * @return list of card info objects {@link model.CardInfo} filtered by validity factor.
     */
    override fun getValidCards(cardNumbers: Iterable<String>): List<CardInfo> {
        return getCardsInfo(cardNumbers).filter { it.isCardValid }
    }

    /**
     * Blocking request to get and filter card info by card network name.
     *
     * @param cardNumbers - card numbers that need to be filtered.
     * @param network - name of card network
     * @return list of card info objects {@link model.CardInfo} filtered by card network name.
     */
    override fun getCardsByNetworkName(cardNumbers: Iterable<String>, network: String): List<CardInfo> {
        return getCardsInfo(cardNumbers).filter { it.network.equals(network, true) }
    }

    /**
     * Blocking request to get and filter card info by card network name.
     *
     * @param cardNumbers - card numbers that need to be filtered.
     * @param filter - predicate with which the list is filtered
     * @return list of card info objects {@link model.CardInfo} filtered by predicate.
     */
    override fun getCardsByPredicate(cardNumbers: Iterable<String>, filter: (CardInfo) -> Boolean): List<CardInfo> {
        return getCardsInfo(cardNumbers).filter { filter.invoke(it) }
    }
}