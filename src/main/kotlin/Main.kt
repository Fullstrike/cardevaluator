fun main(args: Array<String>) {

    val evaluator = Evaluator.create()
    val cardNumbers = listOf(
        "4929804463622139",
        "4929804469812106",
        "6762765696545485",
        "5212132012291762",
        "6210948000000029"
    )

    println("All cards = ${evaluator.getCardsInfo(cardNumbers)}")
    println("Valid cards = ${evaluator.getValidCards(cardNumbers)}")
    println("Card by name = ${evaluator.getCardsByNetworkName(cardNumbers, "visa")}")
    println("Card by predicate = ${evaluator.getCardsByPredicate(cardNumbers) {
        it.isCardValid && it.network.equals(
            "visa",
            true
        )
    }}")
}