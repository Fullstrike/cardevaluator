package model

/**
 * @property number - card number
 * @property isCardValid - boolean represent card validity by this conditions:
 * it contains only numbers and no leading 0
 * it is 12-19 digits long
 * it passes the Luhn check @see https://en.wikipedia.org/wiki/Luhn_algorithm
 * @property isLuhnCheckPassed - if true than card number passed Luhn check  @see https://en.wikipedia.org/wiki/Luhn_algorithm
 * @property network - network name to which the card number belongs
 * @property brand - card number brand name
 */
data class CardInfo(
    val number: String,
    val isCardValid: Boolean = false,
    val isLuhnCheckPassed: Boolean = false,
    val network: String = "",
    val brand: String = ""
)