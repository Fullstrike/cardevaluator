package model

import com.google.gson.annotations.SerializedName

internal data class CardNetworkInfo(
    @SerializedName("number")
    val number: Number,
    @SerializedName("scheme")
    val scheme: String,
    @SerializedName("brand")
    val brand: String
)

internal data class Number(
    @SerializedName("length")
    val length: Int,
    @SerializedName("luhn")
    val luhnCheck: Boolean
)