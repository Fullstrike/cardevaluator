package filter

import model.CardInfo

internal abstract class Filter {

    protected fun isValid(cardNumber: String): Boolean {
        val isCardLengthValid = cardNumber.length in MIN_CARD_LENGTH..MAX_CARD_LENGTH
        val isNoLeadingZero = if (cardNumber.isNotEmpty()) cardNumber.first() != '0' else false
        return isCardLengthValid && isNoLeadingZero
    }
    abstract fun filter(cardNumber: String) : CardInfo

    companion object {
        protected const val MIN_CARD_LENGTH = 12
        protected const val MAX_CARD_LENGTH = 19
    }
}