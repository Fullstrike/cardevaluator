package filter

import model.CardInfo

internal class InternalFilter: Filter() {

    override fun filter(cardNumber: String): CardInfo {
        val luhnCheck = luhnCheck(cardNumber)
        val isCardValid = luhnCheck && isValid(cardNumber)
        val network = CardType.getNetworkName(cardNumber)

        return CardInfo(
            number = cardNumber,
            isCardValid = isCardValid,
            isLuhnCheckPassed = luhnCheck,
            network = network,
            brand = network
        )
    }

    private fun luhnCheck(cardNumber: String): Boolean {
        var sum = 0
        var alternate = false

        cardNumber.reversed().forEach {

            var n = try {
                Integer.parseInt(it.toString())
            } catch (ex: NumberFormatException) {
                return false
            }

            if (alternate) {
                n *= 2
                if (n > 9) {
                    n = n % 10 + 1
                }
            }
            sum += n
            alternate = !alternate
        }

        return sum % 10 == 0
    }
}

private enum class CardType(regexPattern: String) {
    VISA("^4[0-9]{12}(?:[0-9]{3}){0,2}$"),
    MASTERCARD("^(?:5[1-5]|2(?!2([01]|20)|7(2[1-9]|3))[2-7])\\d{14}$"),
    AMERICAN_EXPRESS("^3[47][0-9]{13}$"),
    MAESTRO("^(5612|5893|0604|6390|5018|5020|5038|6304|6759|6761|6762|6763)[0-9]{8,15}\$"),
    DISCOVER("^6(?:011|[45][0-9]{2})[0-9]{12}$"),
    UNION_PAY("^(62[0-9]{14,17})\$");

    private val regex = regexPattern.toRegex()

    companion object {
        fun getNetworkName(cardNumber: String): String {
            for (type in values()) {
                if (type.regex.matches(cardNumber)) {
                    return type.name.toLowerCase()
                }
            }

            return ""
        }
    }
}