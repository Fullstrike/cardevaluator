package filter

import mapper.CardInfoMapper
import model.CardInfo
import network.CardInfoFetcher

internal class NetworkFilter(
    private val fetcher: CardInfoFetcher,
    private val mapper: CardInfoMapper
) : Filter() {

    override fun filter(cardNumber: String): CardInfo {
        val networkResult = fetcher.fetchInfo(cardNumber)
            .let { mapper.mapCardInfoJson(it) }

        with(networkResult) {
            return CardInfo(
                number = cardNumber,
                isCardValid = number.luhnCheck && isValid(cardNumber),
                isLuhnCheckPassed = number.luhnCheck,
                network = scheme,
                brand = brand
            )
        }
    }
}