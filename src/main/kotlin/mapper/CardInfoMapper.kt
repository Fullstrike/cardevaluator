package mapper

import com.google.gson.Gson
import model.CardNetworkInfo

internal class CardInfoMapper(private val gson: Gson) {

    fun mapCardInfoJson(json: String): CardNetworkInfo {
        println("json equals $json")
        return gson.fromJson<CardNetworkInfo>(json, CardNetworkInfo::class.java)
    }
}